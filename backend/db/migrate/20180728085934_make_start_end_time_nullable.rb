class MakeStartEndTimeNullable < ActiveRecord::Migration[5.2]
    def change
        change_column_null :activities, :start_time, true
        change_column_null :activities, :end_time, true
    end
end
