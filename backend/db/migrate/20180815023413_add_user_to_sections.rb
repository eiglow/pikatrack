class AddUserToSections < ActiveRecord::Migration[5.2]
  def change
    add_reference :sections, :user
  end
end
