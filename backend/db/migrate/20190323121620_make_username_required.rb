class MakeUsernameRequired < ActiveRecord::Migration[5.2]
  def change
      User.where(username: nil).each{|u| u.update(username: u.id)}
      change_column_null :users, :username, false
  end
end
