module ApplicationHelper
    def current_unit
        if current_user
            return current_user.setting_unit
        else
            return "metric"
        end
    end
end