class FeedController < ApplicationController
    before_action :authenticate_user!

    def index
        @activities = Activity.includes(:computed_activity)
            .where(user_id: current_user.id)
            .where.not(computed_activities: {id: nil}).order(created_at: :desc)
    end
end
